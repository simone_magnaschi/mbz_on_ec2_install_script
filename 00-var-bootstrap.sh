#!/bin/bash

# Dir on which install the musicbrainz server
MBZ_REPO_DIR_NAME="musicbrainz-server"
MBZ_INSTALL_LOG_FILE="$DIR/install.log"
MBZ_CONTROL_DIR="/root/mbz"
MBZ_LOG_DIR="/var/log/musicbrainz"
TMP_DUMP_DIR="/tmp/dumps"

# Logging separator helper
MSG_SEPARATOR="-----------------------------------"

MBZ_DB_NAME="musicbrainz_db"

# User defined variables
MBZ_INSTALL_DIR="/home/musicbrainz"
DB_SCHEMA_SEQUENCE=23
REPLICATION_TYPE="RT_SLAVE"
MBZ_USER="musicbrainz"
MBZ_PWD="musicbrainz"
POSTGRES_VERSION="9.5"
REPLICATION_ACCESS_TOKEN=""
DOWNLOAD_DATA="1"

# Teardown variables
DROP_DATABASE="1"