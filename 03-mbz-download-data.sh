#!/bin/bash

echo ""
echo $MSG_SEPARATOR
echo " DOWNLOADING DUMP DATA "
echo " AND SETTING UP THE DB "
echo $MSG_SEPARATOR
echo "Downloading Mbz dumps"


mkdir $TMP_DUMP_DIR
cd $TMP_DUMP_DIR
wget $MBZ_FTP_URL/LATEST
LATEST_DUMP_DIR=$(cat LATEST)
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-editor.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-derived.tar.bz2 
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-cdstubs.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-cover-art-archiv3e.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-derived.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-documentation.tar.bz2
wget $MBZ_FTP_URL/$LATEST_DUMP_DIR/mbdump-stats.tar.bz2

echo $MSG_SEPARATOR
echo "Creating and importing database"
echo $MSG_SEPARATOR

$MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/admin/InitDb.pl --createdb --import $TMP_DUMP_DIR/mbdump*.tar.bz2 --echo
rm -rf $TMP_DUMP_DIR
