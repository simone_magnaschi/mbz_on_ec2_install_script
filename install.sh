#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $DIR/00-var-bootstrap.sh

# User request for variables
source $DIR/00-install-variables-request.sh

# DbPrefs Location
MBZ_DB_DEFS_LOCATION="$MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/lib/DBDefs.pm.sample"
MBZ_DB_DEFS_DEST_LOCATION="$MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/lib/DBDefs.pm"

# MBZ FTP URL
MBZ_FTP_URL="ftp.musicbrainz.org/pub/musicbrainz/data/fullexport" #OFFICIAL REPO
#MBZ_FTP_URL="ftp://eu.ftp.musicbrainz.org/MusicBrainz/data/fullexport" #EU MIRROR


echo $MSG_SEPARATOR
echo " MusicBrazinz server setup "
echo " on AWS EC2 Ubuntu AMI "
echo $MSG_SEPARATOR

source $DIR/01-packages-setup.sh
source $DIR/02-mbz-codebase-setup.sh
if [ $DOWNLOAD_DATA -eq 1 ]; then
    source $DIR/03-mbz-download-data.sh
fi
source $DIR/04-setup-nginx-websites.sh
source $DIR/05-mbz-control-scripts.sh

echo "Done! Enjoy!"

exit 0
