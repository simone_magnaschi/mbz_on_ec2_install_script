#!/bin/bash
echo $MSG_SEPARATOR
echo "Create musicbrainz User"
useradd $MBZ_USER -d $MBZ_INSTALL_DIR -g users -m -p `mkpasswd $MBZ_PWD`

mkdir $MBZ_INSTALL_DIR
chown $MBZ_USER:users $MBZ_INSTALL_DIR

echo "Give the guy superuser powers"
echo $MSG_SEPARATOR
echo "$MBZ_USER ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers


echo ""
echo $MSG_SEPARATOR
echo " CLONING AND SETTING UP "
echo " MBZ CODEBASE "
echo $MSG_SEPARATOR
echo "Cloning MBZ codebase"
cd $MBZ_INSTALL_DIR
git clone --recursive git://github.com/metabrainz/musicbrainz-server.git $MBZ_REPO_DIR_NAME
cd $MBZ_REPO_DIR_NAME


echo
echo "Setting up default file"
DBDEFS_FILE_CONTENTS=$( cat ${MBZ_DB_DEFS_LOCATION} )

# Server root
SERVER_ROOT_STRING='# sub MB_SERVER_ROOT    { "/home/httpd/musicbrainz/musicbrainz-server" }'
SERVER_ROOT_STRING_REPLACE="sub MB_SERVER_ROOT { \"$MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME\" }"
DBDEFS_FILE_CONTENTS=${DBDEFS_FILE_CONTENTS//$SERVER_ROOT_STRING/$SERVER_ROOT_STRING_REPLACE}

# Db schema
DB_SCHEMA_STRING='sub DB_SCHEMA_SEQUENCE { [0-9][0-9] }'
DB_SCHEMA_STRING_REPLACE="sub DB_SCHEMA_SEQUENCE { $DB_SCHEMA_SEQUENCE }"
DBDEFS_FILE_CONTENTS=${DBDEFS_FILE_CONTENTS//$DB_SCHEMA_STRING/$DB_SCHEMA_STRING_REPLACE}

# Replication type
REPLICATION_TYPE_STRING='# sub REPLICATION_TYPE { RT_STANDALONE }'
REPLICATION_TYPE_STRING_REPLACE="sub REPLICATION_TYPE { $REPLICATION_TYPE }"
DBDEFS_FILE_CONTENTS=${DBDEFS_FILE_CONTENTS//$REPLICATION_TYPE_STRING/$REPLICATION_TYPE_STRING_REPLACE}

# Replication type
REPLICATION_ACCESS_TOKEN_STRING='# sub REPLICATION_ACCESS_TOKEN { "" }'
REPLICATION_ACCESS_TOKEN_STRING_REPLACE="sub REPLICATION_ACCESS_TOKEN { "$REPLICATION_ACCESS_TOKEN" }"
DBDEFS_FILE_CONTENTS=${DBDEFS_FILE_CONTENTS//$REPLICATION_ACCESS_TOKEN_STRING/$REPLICATION_ACCESS_TOKEN_STRING_REPLACE}

#Save file
echo "$DBDEFS_FILE_CONTENTS" > "$MBZ_DB_DEFS_DEST_LOCATION"



chown $MBZ_USER:users $MBZ_INSTALL_DIR -R

su $MBZ_USER <<EOF
echo "Installing PERL dependencies"
sudo apt-get -y --force-yes install liblocal-lib-perl libxml2-dev libpq-dev libexpat1-dev \
libdb-dev libicu-dev liblocal-lib-perl cpanminus libjson-xs-perl libicu-dev  > /dev/null;
EOF

su $MBZ_USER <<EOF
echo 'eval $( perl -Mlocal::lib )' >> ~/.bashrc
source ~/.bashrc
EOF

su $MBZ_USER <<EOF
echo "MBZ installation"
sudo cpanm --installdeps --notest .
echo "Getting missing cpan modules"
sudo cpanm  MooseX::Role::Parameterized::Meta::Role::Parameterizable
EOF


echo $MSG_SEPARATOR
echo "Setting up postgres"
echo $MSG_SEPARATOR
cd $MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/postgresql-musicbrainz-unaccent
make
sudo make install
cd ..

cd $MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/postgresql-musicbrainz-collate
make
sudo make install
cd ..

echo $MSG_SEPARATOR
echo "Giving dev postgres full permissions (development purposes only!!)"
echo $MSG_SEPARATOR
cp "$DIR/postgres-conf/pg_hba.conf"  "/etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf"
/etc/init.d/postgresql restart
