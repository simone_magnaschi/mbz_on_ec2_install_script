#!/bin/bash

echo ""
echo $MSG_SEPARATOR
echo " SETTING UP THE WEBSITES "
echo $MSG_SEPARATOR
echo "Downloading Mbz dumps"

cd /etc/nginx/sites-available/

echo $MSG_SEPARATOR
echo "Setting up Nginx conf files and Musicbrainz redirect"
echo $MSG_SEPARATOR
cp -f $DIR/nginx-conf/*  "/etc/nginx/sites-available/"

echo $MSG_SEPARATOR
echo "Symlinking musicbrainz-server"
echo $MSG_SEPARATOR
rm -f /etc/nginx/mbserver-rewrites.conf > /dev/null 2&>1
ln -s "$MBZ_INSTALL_DIR/$MBZ_REPO_DIR_NAME/admin/nginx/mbserver-rewrites.conf" /etc/nginx/mbserver-rewrites.conf

echo $MSG_SEPARATOR
echo "Enabling website"
echo $MSG_SEPARATOR
rm -f /etc/nginx/sites-enabled/default > /dev/null 2&>1
rm -f /etc/nginx/sites-enabled/001-musicbrainz > /dev/null 2&>1
rm -f /etc/nginx/sites-enabled/002-adminer > /dev/null 2&>1
ln -s /etc/nginx/sites-available/001-musicbrainz /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/002-adminer /etc/nginx/sites-enabled/

echo $MSG_SEPARATOR
echo "Getting adminer setup"
echo $MSG_SEPARATOR
mkdir /usr/share/adminer
wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php
rm -f /usr/share/adminer/index.php > /dev/null 2&>1
ln -s /usr/share/adminer/latest.php /usr/share/adminer/index.php

echo $MSG_SEPARATOR
echo "Restarting Postgres and Nginx"
echo $MSG_SEPARATOR
/etc/init.d/postgresql restart
/etc/init.d/nginx restart
