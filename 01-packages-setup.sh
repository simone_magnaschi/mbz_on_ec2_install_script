#!/bin/bash


echo $MSG_SEPARATOR
echo " INSTALLING PACKAGES "
echo $MSG_SEPARATOR

echo $MSG_SEPARATOR
echo "Updating packages and update system"
echo $MSG_SEPARATOR

# Add repository for PostGres 9.5
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

apt-get update > /dev/null;
apt-get -yq --force-yes upgrade;
apt-get -yq --force-yes dist-upgrade;

echo $MSG_SEPARATOR
echo "Installing utilities"
echo $MSG_SEPARATOR
apt-get -yq --force-yes install htop iotop iftop nano whois > /dev/null;

echo $MSG_SEPARATOR
echo "Installing Postgres"
echo $MSG_SEPARATOR
apt-get -yq --force-yes install postgresql-$POSTGRES_VERSION postgresql-server-dev-$POSTGRES_VERSION \
postgresql-contrib-$POSTGRES_VERSION postgresql-plperl-$POSTGRES_VERSION  > /dev/null;

echo $MSG_SEPARATOR
echo "Installing Git, Memcache, Redis"
echo $MSG_SEPARATOR
apt-get -yq --force-yes install git-core memcached redis-server  > /dev/null;

echo $MSG_SEPARATOR
echo "Installing Build Tools"
echo $MSG_SEPARATOR
apt-get -yq --force-yes install build-essential  > /dev/null;

echo $MSG_SEPARATOR
echo "Installing PHP-FPM + Nginx"
echo $MSG_SEPARATOR
apt-get -yq --force-yes install nginx php5-fpm php5-cli php5-pgsql  > /dev/null;

