# Music Brainz EC2 Install script 

This repository contains a collection of bash scripts to sort of automate the installation and configuration of a musicbrainz slave server on Amazone EC2.

It is meant to be used for development purposes only, since security has not been taken into any consideration right now. Basically it is a convenient bash script that gets and ties everything together, installs everything that's needed and let you familiarize with the server and the db structure.

It has been tested with the **Ubuntu Server 14.04 LTS (HVM), SSD Volume Type** AMI.

At least use a t2.small instance, I was never been able to get it to work continuosly on a t2.micro (after some time it just became unresponsive).
Be sure to give it 40Gb of disk space since the database is huge.

Spin up a new EC2 instance and when the prompt is ready run this code:

    sudo su
    apt-get update > /dev/null;
    apt-get install git -y < "/dev/null"
    cd ~
    git clone https://simone_magnaschi@bitbucket.org/simone_magnaschi/mbz_on_ec2_install_script.git
    chmod +x mbz_on_ec2_install_script/*.sh
    ./mbz_on_ec2_install_script/install.sh


It will:

- update the various distro packages
- install common utilities
- install postrgresql, git, memcached, redis, php5-fpm + nginx
- install the musicbrainz server, fetch the latest MBZ data via FTP and import it in the database
- clone the mbz server repo, install all the perl dependencies and basically get it to work
- install the base server and web services on port 80
- install adminer and get it available on port 8080
- install supervisord to automate the startup of mbz server
- set up the cron jobs
 
**Disclaimer**

I'm no sysadmin or a bash expert. The code is ugly but it works for my purposes. In case you want to contribute and make it better, you're welcome! 