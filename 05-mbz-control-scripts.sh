#!/bin/bash

mkdir $MBZ_LOG_DIR

echo $MSG_SEPARATOR
echo "Creating start stop script for Mbz - mbacontrol"
echo $MSG_SEPARATOR
cp -f $DIR/mbz-control-scripts/mbcontrol  "/usr/bin/"
chmod +x /usr/bin/mbcontrol

echo $MSG_SEPARATOR
echo "Installing Supervisord"
echo $MSG_SEPARATOR
apt-get install -yq --force-yes supervisor > /dev/null;
mkdir /var/log/mbz
cp -f $DIR/mbz-control-scripts/mbz_supervisord.conf  "/etc/supervisor/conf.d/mbz.conf"

echo $MSG_SEPARATOR
echo "Creating system startup script"
echo $MSG_SEPARATOR
rm -rf $MBZ_CONTROL_DIR

mkdir $MBZ_CONTROL_DIR > /dev/null 2&>1
cp -f $DIR/mbz-control-scripts/mbz_startup_script.sh $MBZ_CONTROL_DIR
chmod +x $MBZ_CONTROL_DIR/mbz_startup_script.sh

echo $MSG_SEPARATOR
echo "Adding Cron scripts"
echo $MSG_SEPARATOR

for COMMAND in "10 * * * * /usr/bin/mbcontrol hourly"
do
	echo $COMMAND
	CTMPESC=$(sed 's/[\*\.&/]/\\&/g' <<<"$COMMAND")
	crontab -l | sed "/$CTMPESC/d" | crontab -
	(crontab -u root -l; echo "$COMMAND" ) | crontab -u root -
done

updatedb
