#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $DIR/00-var-bootstrap.sh

# User request for variables
source $DIR/00-teardown-variables-request.sh

userdel $MBZ_USER
rm -rf $MBZ_INSTALL_DIR

if [ $DROP_DATABASE -eq 1 ]; then
    dropdb -h localhost -p 5432 -U postgress $MBZ_DB_NAME
fi

echo "Done! Enjoy!"

exit 0
