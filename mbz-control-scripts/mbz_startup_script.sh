#!/bin/bash

/usr/bin/mbcontrol stop  > /dev/null 2&>1
rm /var/run/musicbrainz/server.pid  > /dev/null 2&>1
mkdir /var/run/musicbrainz > /dev/null 2&>1
/usr/bin/mbcontrol start
